package pw.elka.sag.wedt;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.routing.RoundRobinPool;
import pw.elka.sag.wedt.broker.BrokerActor;
import pw.elka.sag.wedt.common.model.OfficeSearchArguments;
import pw.elka.sag.wedt.service.ServiceActor;
import pw.elka.sag.wedt.service.research.GumtreeResearch;
import pw.elka.sag.wedt.service.research.OlxResearch;
import pw.elka.sag.wedt.user.UserActor;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class OfficeRentSearch {

    private static final int BROKER_ACTORS_COUNT = 5;

    private static ActorSystem actorSystem;
    private static ActorRef brokerPool;

    public static void main(String[] args) {
        init();
        OfficeSearchArguments officeSearchArguments = askForOfficeSearchArguments();
        final ActorRef userActor = actorSystem.actorOf(UserActor.props(officeSearchArguments, brokerPool), "userActor1");
        userActor.tell(new UserActor.FindOffices(), ActorRef.noSender());
    }

    private static void init() {
        actorSystem = ActorSystem.create("actorSystem");
        final ActorRef olxActor =
                actorSystem.actorOf(ServiceActor.props(new OlxResearch()), "olxActor"); // pool instead?
        final ActorRef gumtreeActor =
                actorSystem.actorOf(ServiceActor.props(new GumtreeResearch()), "gumtreeActor"); //pool instead?
        brokerPool = actorSystem.actorOf(new RoundRobinPool(BROKER_ACTORS_COUNT).props(BrokerActor.props(Arrays.asList(gumtreeActor, olxActor))), "brokers"); //BalancingPoolRouter instead?
    }

    private static OfficeSearchArguments askForOfficeSearchArguments() {
        Scanner reader = new Scanner(System.in);
        String line = "";
        Integer minPrice = null, maxPrice = null, minSurface = null, maxSurface = null, minFloor = null, maxFloor = null;
        System.out.println("Witaj w wyszukiwarce ofert wynajmu biur w Warszawie!");

        System.out.println("Podaj minimalną cenę");
        line = reader.nextLine();
        if (!line.isEmpty())
            minPrice = Integer.parseInt(line);

        System.out.println("Podaj maksymalną cenę");
        line = reader.nextLine();
        if (!line.isEmpty())
            maxPrice = Integer.parseInt(line);

        System.out.println("Podaj minimalną powierzchnię");
        line = reader.nextLine();
        if (!line.isEmpty())
            minSurface = Integer.parseInt(line);

        System.out.println("Podaj maksymalną powierzchnię");
        line = reader.nextLine();
        if (!line.isEmpty())
            maxSurface = Integer.parseInt(line);

        System.out.println("Podaj minimalne piętro");
        line = reader.nextLine();
        if (!line.isEmpty())
            minFloor = Integer.parseInt(line);

        System.out.println("Podaj maksymalne piętro");
        line = reader.nextLine();
        if (!line.isEmpty())
            maxFloor = Integer.parseInt(line);

        System.out.println("Podaj listę dzielnic, oddzielone przecinkiem");
        String districtsS = reader.nextLine();
        List<String> districts = null;
        if (districtsS != null && !districtsS.equals("")) {
            districtsS = districtsS.replaceAll("\\s+", "");
            districts = Arrays.asList(districtsS.split("\\s*,\\s*"));
        }
        return new OfficeSearchArguments(minPrice, maxPrice, minSurface, maxSurface, minFloor, maxFloor, districts);
    }
}
