package pw.elka.sag.wedt.user;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import pw.elka.sag.wedt.broker.BrokerActor;
import pw.elka.sag.wedt.common.model.OfficeOffer;
import pw.elka.sag.wedt.common.model.OfficeSearchArguments;

import java.util.ArrayList;
import java.util.List;

public class UserActor extends AbstractActor {

    static public Props props(OfficeSearchArguments searchArguments, ActorRef brokerActor) {
        return Props.create(UserActor.class, () -> new UserActor(searchArguments, brokerActor));
    }

    // messages in
    static public class FindOffices {
        public FindOffices() {
        }
    }

    static public class FoundOffers {
        public final List<OfficeOffer> officeOffers;

        public FoundOffers(List<OfficeOffer> officeOffers) {
            this.officeOffers = officeOffers;
        }
    }

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    private final OfficeSearchArguments officeSearchArguments;
    private final ActorRef brokerActor;
    private List<OfficeOffer> officeOffers = new ArrayList<>();

    private UserActor(OfficeSearchArguments officeSearchArguments, ActorRef brokerActor) {
        this.officeSearchArguments = officeSearchArguments;
        this.brokerActor = brokerActor;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(FindOffices.class, x -> {
                    this.brokerActor.tell(new BrokerActor.FindOffices(this.officeSearchArguments), getSelf());
                })
                .match(FoundOffers.class, offers -> {
                    this.officeOffers.addAll(offers.officeOffers);
                    log.info(self().path().name() + " found offers:");
                    offers.officeOffers.forEach(offer -> {
                        log.info("\n" + offer.toString());
                    });
                })
                .build();
    }
}
