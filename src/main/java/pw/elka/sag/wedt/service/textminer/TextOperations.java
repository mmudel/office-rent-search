package pw.elka.sag.wedt.service.textminer;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;

public class TextOperations {

    public static boolean isNumericValue(String word) {
        for (char c : word.toCharArray()) {
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }

    public static boolean contains(List<String> list, String word) {
        for (String element : list) {
            if (element.equals(word)) return true;
        }
        return false;
    }

    public static String[] splitText(String text) {
        ArrayList<String> results = new ArrayList<>();
        BreakIterator breakIterator = BreakIterator.getWordInstance();
        breakIterator.setText(text);
        int firstIdx;
        int lastIdx = breakIterator.first();
        while (lastIdx != BreakIterator.DONE) {
            firstIdx = lastIdx;
            lastIdx = breakIterator.next();
            if (lastIdx != BreakIterator.DONE && Character.isLetterOrDigit(text.charAt(firstIdx))) {
                String word = text.substring(firstIdx, lastIdx).toLowerCase();
                String endUnit = getEndUnit(word);
                if (endUnit != null && getNumericValue(word, endUnit.length()) != null) {
                    results.add(getNumericValue(word, endUnit.length()));
                    results.add(endUnit);
                } else {
                    results.add(word);
                }
            }
        }
        return results.toArray(new String[results.size()]);
    }

    public static String toUpperFirstCharacter(String word) {
        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }

    public static String getEndUnit(String word) {
        Parameter[] parameters = {Parameter.PRICE, Parameter.SURFACE};
        for (Parameter parameter : parameters) {
            for (String element : PolishDictionary.getUnitSearchList(parameter)) {
                if (word.endsWith(element)) {
                    return element;
                }
            }
        }
        return null;
    }

    public static String getNumericValue(String word, int sizeOfEndUnit) {
        if (sizeOfEndUnit > 0 && word.length() > sizeOfEndUnit) {
            String value = word.substring(0, word.length() - sizeOfEndUnit);
            if (isNumericValue(value)) {
                return value;
            }
        }
        return null;
    }

}
