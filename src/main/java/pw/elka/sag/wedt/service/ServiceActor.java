package pw.elka.sag.wedt.service;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import pw.elka.sag.wedt.common.model.OfficeSearchArguments;
import pw.elka.sag.wedt.service.common.model.SearchOffices;
import pw.elka.sag.wedt.user.UserActor;

public class ServiceActor extends AbstractActor {

    static public Props props(SearchOffices officesResearcher) {
        return Props.create(ServiceActor.class, () -> new ServiceActor(officesResearcher));
    }

    // messages in
    static public class FindOffices {
        public final OfficeSearchArguments officeSearchArguments;
        public final ActorRef userActor;

        public FindOffices(OfficeSearchArguments officeSearchArguments, ActorRef userActor) {
            this.officeSearchArguments = officeSearchArguments;
            this.userActor = userActor;
        }
    }

    private final SearchOffices officesResearcher;

    private ServiceActor(SearchOffices officesResearcher) {
        this.officesResearcher = officesResearcher;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(FindOffices.class, request -> {
                    request.userActor.tell(new UserActor.FoundOffers(this.officesResearcher.search(request.officeSearchArguments)), ActorRef.noSender());
                })
                .build();
    }
}
