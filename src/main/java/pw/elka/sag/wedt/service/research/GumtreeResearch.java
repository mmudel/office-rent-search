package pw.elka.sag.wedt.service.research;

import pw.elka.sag.wedt.common.model.OfficeDetails;
import pw.elka.sag.wedt.common.model.OfficeOffer;
import pw.elka.sag.wedt.common.model.OfficeSearchArguments;
import pw.elka.sag.wedt.service.common.model.SearchOffices;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import java.io.IOException;
import java.util.*;
import java.net.URL;

import org.jsoup.nodes.Document;
import pw.elka.sag.wedt.service.textminer.TextMiner;

public class GumtreeResearch implements SearchOffices {
    @Override
    public List<OfficeOffer> search(OfficeSearchArguments officeSearchArguments) {
        final int MAX_PAGES = 2; // Should be increased, if everything else is working correctly
        List<OfficeOffer> officeOffers = new ArrayList<>();

        try {
            String page = new Scanner(new URL("https://www.gumtree.pl/s-lokal-i-biuro/warszawa/v1c9072l3200008p1").openStream(), "UTF-8").useDelimiter("\\A").next();
            Document doc = Jsoup.parse(page);

            Elements offers = doc.select("a[href^=/a-lokal-i-biuro]");

            HashSet<String> links = new HashSet<>(); // set container to enforce link uniqueness
            for (Element offer : offers) {
                links.add("https://www.gumtree.pl" + offer.attr("href"));
            }

            for (int i = 2; i <= MAX_PAGES; ++i) {
                page = new Scanner(new URL("https://www.gumtree.pl/s-lokal-i-biuro/warszawa/page-" + Integer.toString(i) + "/v1c9072l3200008p13").openStream(), "UTF-8").useDelimiter("\\A").next();
                doc = Jsoup.parse(page);

                offers = doc.select("a[href^=/a-lokal-i-biuro]");

                for (Element offer : offers) {
                    links.add("https://www.gumtree.pl" + offer.attr("href"));
                }
            }

            TextMiner miner = new TextMiner();

            for (String link : links) {
                page = new Scanner(new URL(link).openStream(), "UTF-8").useDelimiter("\\A").next();
                doc = Jsoup.parse(page);
                String price = doc.select("div.price").first().text() + " ";
                Element firstDiv = doc.select("div.vip-details").first();
                if (firstDiv == null)
                    continue;
                String title = doc.select("title").first().text();
                String offerText = price + firstDiv.text();
                OfficeDetails o = miner.start(offerText);

                if (officeSearchArguments.conditionsMet(o))
                    officeOffers.add(new OfficeOffer(title, link, o));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return officeOffers;
    }
}
