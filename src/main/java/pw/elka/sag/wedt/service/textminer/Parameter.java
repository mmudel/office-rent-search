package pw.elka.sag.wedt.service.textminer;


public enum Parameter {
    DISTRICT,
    SURFACE,
    PRICE,
    FLOOR
}
