package pw.elka.sag.wedt.service.research;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pw.elka.sag.wedt.common.model.OfficeDetails;
import pw.elka.sag.wedt.common.model.OfficeOffer;
import pw.elka.sag.wedt.common.model.OfficeSearchArguments;
import pw.elka.sag.wedt.service.common.model.SearchOffices;

import java.io.IOException;
import java.util.*;
import java.net.URL;

import org.jsoup.nodes.Document;
import pw.elka.sag.wedt.service.textminer.TextMiner;

public class OlxResearch implements SearchOffices {
    @Override
    public List<OfficeOffer> search(OfficeSearchArguments officeSearchArguments) {
        final int MAX_PAGES = 2; // Should be increased, if everything else is working correctly

        List<OfficeOffer> officeOffers = new ArrayList<>();

        try {
            String page = new Scanner(new URL("https://www.olx.pl/nieruchomosci/biura-lokale/warszawa/").openStream(), "UTF-8").useDelimiter("\\A").next();
            Document doc = Jsoup.parse(page);


            doc.select("a.detailsLinkPromoted").remove(); // remove some ad-links
            Elements offers = doc.select("a[href^=https://www.olx.pl/oferta]");

            HashSet<String> links = new HashSet<>(); // set container to enforce link uniqueness
            for (Element offer : offers) {
                links.add(offer.attr("href"));
            }

            for (int i = 2; i <= MAX_PAGES; ++i) {
                page = new Scanner(new URL("https://www.olx.pl/nieruchomosci/biura-lokale/wynajem/warszawa/?page=" + Integer.toString(i)).openStream(), "UTF-8").useDelimiter("\\A").next();


                doc = Jsoup.parse(page);

                doc.select("a.detailsLinkPromoted").remove(); // remove some ad-links
                offers = doc.select("a[href^=https://www.olx.pl/oferta]");

                for (Element offer : offers) {
                    links.add(offer.attr("href"));
                }
            }

            TextMiner miner = new TextMiner();

            for (String link : links) {
                page = new Scanner(new URL(link).openStream(), "UTF-8").useDelimiter("\\A").next();
                doc = Jsoup.parse(page);
                String price = doc.select("div.price-label").first().text() + " ";
                Element firstDiv = doc.select("div.offercontentinner").first();
                if (firstDiv == null)
                    continue;
                String title = doc.select("title").first().text();
                String offerText = price + firstDiv.text();
                OfficeDetails o = miner.start(offerText);

                if (officeSearchArguments.conditionsMet(o))
                    officeOffers.add(new OfficeOffer(title, link, o));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return officeOffers;
    }
}
