package pw.elka.sag.wedt.service.textminer;

import java.util.List;


public class Result {

    private int wordIndex;
    private List<String> stems;

    public Result(int wordIndex, List<String> stems) {
        this.wordIndex = wordIndex;
        this.stems = stems;
    }

    public int getWordIndex() {
        return wordIndex;
    }

    public void setWordIndex(int wordIndex) {
        this.wordIndex = wordIndex;
    }

    public List<String> getStems() {
        return stems;
    }

    public void setStems(List<String> stems) {
        this.stems = stems;
    }


}
