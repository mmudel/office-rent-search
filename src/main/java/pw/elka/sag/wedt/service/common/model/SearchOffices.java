package pw.elka.sag.wedt.service.common.model;

import pw.elka.sag.wedt.common.model.OfficeOffer;
import pw.elka.sag.wedt.common.model.OfficeSearchArguments;

import java.util.List;

public interface SearchOffices {
    List<OfficeOffer> search(OfficeSearchArguments officeSearchArguments);
}
