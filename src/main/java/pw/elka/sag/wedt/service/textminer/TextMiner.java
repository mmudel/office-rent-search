package pw.elka.sag.wedt.service.textminer;

import morfologik.stemming.WordData;
import morfologik.stemming.polish.PolishStemmer;
import pw.elka.sag.wedt.common.model.OfficeDetails;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TextMiner {

    private PolishStemmer stemmer = new PolishStemmer();
    private OfficeDetails officeDetails;
    private String[] words;
    private boolean[] foundParameters = new boolean[Parameter.values().length];

    public OfficeDetails start(String text) {
        officeDetails = new OfficeDetails();
        words = TextOperations.splitText(text);
        Arrays.fill(foundParameters, false);
        for (int i = 0; i < words.length; ++i) {
            Result result = new Result(i, stem(words[i]));
            for (Parameter parameter : Parameter.values()) {
                if (Parameter.DISTRICT.equals(parameter)) {
                    findParameter(new Result(i, stem(TextOperations.toUpperFirstCharacter(words[i]))), parameter);
                } else {
                    findParameter(result, parameter);
                }
            }
        }
        return officeDetails;
    }

    private void findParameter(Result result, Parameter parameter) {
        String originalWord = words[result.getWordIndex()];
        List<String> stems = result.getStems();
        if (!foundParameters[parameter.ordinal()]) {
            if (Parameter.FLOOR.equals(parameter)) {
                if (TextOperations.contains(stems, PolishDictionary.groundFloorName)) {
                    officeDetails.setFloor(0);
                    return;
                }
            }
            if (Parameter.PRICE.equals(parameter) || Parameter.SURFACE.equals(parameter)) {
                for (String element : PolishDictionary.getUnitSearchList(parameter)) {
                    if (originalWord.equals(element)) {
                        Integer value = getNumericValueBeside(result.getWordIndex(), true);
                        if (Parameter.PRICE.equals(parameter)) {
                            officeDetails.setPrice(value);
                        } else {
                            officeDetails.setSurface(value);
                        }
                        foundParameters[parameter.ordinal()] = value != null;
                    }
                }
            }
            for (String element : PolishDictionary.getLabelSearchList(parameter)) {
                if (TextOperations.contains(stems, element)) {
                    switch (parameter) {
                        case PRICE:
                            Integer value = getNumericValueBeside(result.getWordIndex(), true);
                            officeDetails.setPrice(value);
                            foundParameters[parameter.ordinal()] = value != null;
                            break;
                        case SURFACE:
                            value = getNumericValueBeside(result.getWordIndex(), false);
                            officeDetails.setSurface(value);
                            foundParameters[parameter.ordinal()] = value != null;
                            break;
                        case DISTRICT:
                            officeDetails.setDistrict(element);
                            foundParameters[parameter.ordinal()] = true;
                            break;
                        case FLOOR:
                            value = getNumericValueBeside(result.getWordIndex(), false);
                            value = value == null ? getGroundFloorByName(result.getWordIndex()) : value;
                            if (value != null) {
                                officeDetails.setFloor(value);
                            }
                            break;
                    }
                }
            }
        }
    }

    private Integer getGroundFloorByName(int index) {
        String word;
        word = words[index + 1];
        if (!PolishDictionary.groundFloorName.equals(word)) {
            word = words[index - 1];
            return PolishDictionary.groundFloorName.equals(word) ? 0 : null;
        }
        return 0;
    }

    private Integer getNumericValueBeside(int index, boolean previousValueFirst) {
        String value = "";
        int currIdx = index;
        currIdx = previousValueFirst ? --currIdx : ++currIdx;
        String currValue = getNumericValue(currIdx);
        while (currValue != null) {
            value = previousValueFirst ? currValue + value : value + currValue;
            currIdx = previousValueFirst ? --currIdx : ++currIdx;
            currValue = getNumericValue(currIdx);
        }
        if (value.length() == 0) {
            currIdx = index;
            currIdx = previousValueFirst ? ++currIdx : --currIdx;
            currValue = getNumericValue(currIdx);
            while (currValue != null) {
                value = previousValueFirst ? value + currValue : currValue + value;
                currIdx = previousValueFirst ? ++currIdx : --currIdx;
                currValue = getNumericValue(currIdx);
            }
        }
        return value.length() == 0 ? null : Integer.valueOf(value);
    }

    private String getNumericValue(int index) {
        if (index >= 0 && index < words.length) {
            String word = words[index];
            if (word != null) {
                if (word.contains(",")) word = word.substring(0, word.indexOf(","));
                if (word.contains(".")) word = word.substring(0, word.indexOf("."));
                if (TextOperations.isNumericValue(word)) return word;
            }
        }
        return null;
    }

    private List<String> stem(String word) {
        List<String> results = new ArrayList<>();
        for (WordData wordData : stemmer.lookup(word)) {
            results.add(wordData.getStem().toString());
        }
        return results;
    }

}
