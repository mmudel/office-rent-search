package pw.elka.sag.wedt.service.textminer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PolishDictionary {
    public static List<String> priceLabels = Arrays.asList("złoty");
    public static List<String> priceUnits = Arrays.asList("zł", "zl", "pln", "(zł)");
    public static List<String> surfaceLabels = Arrays.asList("powierzchnia", "wielkość");
    public static List<String> surfaceUnits = Arrays.asList("m2", "m²", "m.kw", "mkw", "(m2)");
    public static List<String> districtLabels = Arrays.asList("Białołęka", "Bielany", "Bemowo", "Ursus", "Żoliborz", "Ochota",
            "Praga", "Śródmieście", "Wola", "Ursynów", "Mokotów", "Wilanów", "Wawer", "Wesoła", "Rembertów", "Targówek");
    public static List<String> floorLabels = Arrays.asList("piętro", "poziom");
    public static String groundFloorName = "parter";


    public static List<String> getLabelSearchList(Parameter parameter) {
        switch (parameter) {
            case PRICE:
                return priceLabels;
            case SURFACE:
                return surfaceLabels;
            case DISTRICT:
                return districtLabels;
            case FLOOR:
                return floorLabels;
            default:
                return new ArrayList<>();
        }

    }

    public static List<String> getUnitSearchList(Parameter parameter) {
        switch (parameter) {
            case PRICE:
                return priceUnits;
            case SURFACE:
                return surfaceUnits;
            default:
                return new ArrayList<>();
        }
    }

}
