package pw.elka.sag.wedt.common.model;

import java.util.List;

public class OfficeSearchArguments {
    private Integer minPrice;
    private Integer maxPrice;
    private Integer minSurface;
    private Integer maxSurface;
    private Integer minFloor;
    private Integer maxFloor;
    private List<String> districts;

    public OfficeSearchArguments(Integer minPrice, Integer maxPrice, Integer minSurface, Integer maxSurface, Integer minFloor, Integer maxFloor, List<String> districts) {
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.minSurface = minSurface;
        this.maxSurface = maxSurface;
        this.minFloor = minFloor;
        this.maxFloor = maxFloor;
        this.districts = districts;
    }

    public int getMinFloor() {
        return minFloor;
    }

    public void setMinFloor(int minFloor) {
        this.minFloor = minFloor;
    }

    public int getMaxFloor() {
        return maxFloor;
    }

    public void setMaxFloor(int maxFloor) {
        this.maxFloor = maxFloor;
    }

    public List<String> getDistricts() {
        return districts;
    }

    public void setDistricts(List<String> districts) {
        this.districts = districts;
    }

    public Integer getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Integer minPrice) {
        this.minPrice = minPrice;
    }

    public Integer getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Integer maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Integer getMinSurface() {
        return minSurface;
    }

    public void setMinSurface(Integer minSurface) {
        this.minSurface = minSurface;
    }

    public Integer getMaxSurface() {
        return maxSurface;
    }

    public void setMaxSurface(Integer maxSurface) {
        this.maxSurface = maxSurface;
    }

    public boolean conditionsMet(OfficeDetails od) {
        if (minPrice != null && (od.getPrice() == null || od.getPrice() < minPrice))
            return false;
        if (maxPrice != null && (od.getPrice() == null || od.getPrice() > maxPrice))
            return false;
        if (minSurface != null && (od.getSurface() == null || od.getSurface() < minSurface))
            return false;
        if (maxSurface != null && (od.getSurface() == null || od.getSurface() > maxSurface))
            return false;
        if (minFloor != null && (od.getFloor() == null || od.getFloor() < minFloor))
            return false;
        if (maxFloor != null && (od.getFloor() == null || od.getFloor() > maxFloor))
            return false;
        if (districts != null && (od.getDistrict() == null || !districts.contains(od.getDistrict())))
            return false;
        return true;
    }
}
