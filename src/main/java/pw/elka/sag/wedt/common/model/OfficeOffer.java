package pw.elka.sag.wedt.common.model;


public class OfficeOffer {
    private String title;
    private String link;
    private OfficeDetails officeDetails;

    public OfficeOffer(String title, String link, OfficeDetails officeDetails) {
        this.title = title;
        this.link = link;
        this.officeDetails = officeDetails;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public OfficeDetails getOfficeDetails() {
        return officeDetails;
    }

    public void setOfficeDetails(OfficeDetails officeDetails) {
        this.officeDetails = officeDetails;
    }

    public String toString() {
        return title + "\n" + link + "\n" + officeDetails.toString() + "\n";
    }
}
