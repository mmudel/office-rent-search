package pw.elka.sag.wedt.common.model;


public class OfficeDetails {

    private Integer price;
    private Integer surface;
    private Integer floor;
    private String district;

    public OfficeDetails() {
    }

    public OfficeDetails(Integer price, Integer surface, Integer floor, String district) {
        this.price = price;
        this.surface = surface;
        this.floor = floor;
        this.district = district;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getSurface() {
        return surface;
    }

    public void setSurface(Integer surface) {
        this.surface = surface;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String toString() {
        return "Price: " + price + "\nSurface: " + surface + "\nFloor: " + floor + "\nDistrict: " + district;
    }
}
