package pw.elka.sag.wedt;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.routing.RoundRobinPool;
import pw.elka.sag.wedt.broker.BrokerActor;
import pw.elka.sag.wedt.common.model.OfficeSearchArguments;
import pw.elka.sag.wedt.service.ServiceActor;
import pw.elka.sag.wedt.service.research.GumtreeResearch;
import pw.elka.sag.wedt.service.research.OlxResearch;
import pw.elka.sag.wedt.user.UserActor;

import java.util.*;

public class OfficeRentSearchSimulation {
    private static final int BROKER_ACTORS_COUNT = 5;
    private static final int USERS = 10;

    private static ActorSystem actorSystem;
    private static ActorRef brokerPool;


    public static void main(String[] args) {
        init();
        for (int i = 0; i < USERS; i++) {
            final ActorRef userActor = actorSystem.actorOf(UserActor.props(generateOfficeSearchArguments(), brokerPool), "userActor" + i);
            userActor.tell(new UserActor.FindOffices(), ActorRef.noSender());
        }
    }

    private static void init() {
        actorSystem = ActorSystem.create("actorSystem");
        final ActorRef olxActor =
                actorSystem.actorOf(ServiceActor.props(new OlxResearch()), "olxActor"); // pool instead?
        final ActorRef gumtreeActor =
                actorSystem.actorOf(ServiceActor.props(new GumtreeResearch()), "gumtreeActor"); // pool instead?
        brokerPool = actorSystem.actorOf(new RoundRobinPool(BROKER_ACTORS_COUNT).props(BrokerActor.props(Arrays.asList(gumtreeActor, olxActor))), "brokers"); //BalancingPoolRouter instead?
    }

    private static OfficeSearchArguments generateOfficeSearchArguments() {
        Random rand = new Random();
        return new OfficeSearchArguments((int) (rand.nextDouble() * 4000 + 2000), (int) (rand.nextDouble() * 4000 + 6000), (int) (rand.nextDouble() * 40 + 30), (int) rand.nextDouble() * 40 + 70,
                rand.nextInt(2), rand.nextInt(6) + 4, new ArrayList<>());
    }
}
