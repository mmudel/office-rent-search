package pw.elka.sag.wedt.broker;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import pw.elka.sag.wedt.common.model.OfficeSearchArguments;
import pw.elka.sag.wedt.service.ServiceActor;

import java.util.List;

public class BrokerActor extends AbstractActor {

    static public Props props(List<ActorRef> serviceActors) {
        return Props.create(BrokerActor.class, () -> new BrokerActor(serviceActors));
    }

    // messages in
    static public class FindOffices {
        public final OfficeSearchArguments officeSearchArguments;

        public FindOffices(OfficeSearchArguments officeSearchArguments) {
            this.officeSearchArguments = officeSearchArguments;
        }
    }

    private final List<ActorRef> serviceActors;

    private BrokerActor(List<ActorRef> serviceActors) {
        this.serviceActors = serviceActors;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(FindOffices.class, request -> {
                    serviceActors.forEach(serviceActor -> serviceActor.tell(new ServiceActor.FindOffices(request.officeSearchArguments, getSender()), getSelf()));
                })
                .build();
    }
}
